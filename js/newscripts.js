$(function () {
  var $newSlider = $('.js-new-slider');
  if(!$newSlider.length) return;

  $newSlider.slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
  });
});

$(function () {
  var $submenuBtn = $('.js-mob-submenu-btn');
  if(!$submenuBtn.length) return;

  $submenuBtn.click(function(){
    $(this).toggleClass('active');
    return false;
  });
});

$(function () {
  var $newSliderBrand = $('.js-new-brand-slider');
  if(!$newSliderBrand.length) return;

  $newSliderBrand.slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 6,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.js-tabs a').click(function(){
    $(this).parents('.tab-wrap').find('.tab-cont-js').addClass('hide');
    $(this).parent().siblings().removeClass('active');
    var id = $(this).attr('href');
    $(id).removeClass('hide');
    $(this).parent().addClass('active');
    return false;
  });
});

$(function(){
  $('.js-links-box-btn').click(function(){
    $(this).prev('.new-links-box').addClass('open');
    $(this).addClass('close');
    return false;
  });
});